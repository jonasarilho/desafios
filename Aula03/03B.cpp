#include <bits/stdc++.h>

using namespace std;

void task() {
    int n, d;
    long long *a, sum = 0;

    scanf("%d %d", &n, &d);
    a = (long long*)calloc(n, sizeof(long long));

    for (int i = 0; i < n; i++) {
        scanf("%lld", &a[i]);
        sum += a[i];
    }

    if(sum%n) {
        printf("-1\n");
        return;
    }

    long long res = 0, aux;
    sum /= n;
    for (int i = 0; i < d; i++) {
        int j;
        for (j = i; j < n - d; j+= d) {
            aux = sum - a[j];
            res += abs(aux);
            a[j] = sum;
            a[j+d] -= aux;
        }
        if(a[j] != sum) {
            printf("-1\n");
            return;
        }
    }

    printf("%lld\n", res);

    free(a);
    return;
}

int main(int argc, char const *argv[]) {

    int t;

    scanf("%d", &t);

    for (int i = 0; i < t; i++) {
        task();
    }


    return 0;
}
