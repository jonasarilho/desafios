#include "bits/stdc++.h"

using namespace std;

const int MAXN = 20001;
const int MAXK = 5001;

void remove_edge(vector<int> &parent, int a) {

    parent[a] = 0;
}


bool exists_path(vector<int> &parent, int a, int b) {

    while(parent[a]) a = parent[a];
    while(parent[b]) b = parent[b];
    if(a == b) return true;

    return false;
}

void task(int t) {

    int n, k;
    scanf("%d %d", &n, &k);
    vector<int> parent(n+1);
    for (int i = 1; i <= n; i++) scanf("%d", &parent[i]);

    vector<int> ans(k);

    for (int i = 0; i < k; i++) {
        char c;
        getchar();
        scanf("%c", &c);
        if(c == 'C') {
            int a;
            scanf("%d", &a);
            remove_edge(parent, a);
            ans[i] = 0;
        }
        else {
            int a, b;
            scanf("%d %d", &a, &b);
            if(exists_path(parent, a, b)) ans[i] = 1;
            else ans[i] = -1;
        }
    }

    printf("Case #%d:\n", t);
    for (int i = 0; i < k; i++) {
        if(ans[i] == 1) printf("YES\n");
        else if (ans[i] == -1) printf("NO\n");
    }

}


int main(int argc, char const *argv[]) {

    int t;

    scanf("%d", &t);

    for (int i = 0; i < t; i++) {
        task(i+1);
    }

    return 0;
}
