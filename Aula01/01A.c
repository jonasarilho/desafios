#include <stdio.h>
#include <stdlib.h>
#include <string.h>


void print_num(int a) {
  if(a == 1) printf("one\n");
  else if(a == 2) printf("two\n");
  else if(a == 3) printf("three\n");
  else if(a == 4) printf("four\n");
  else if(a == 5) printf("five\n");
  else if(a == 6) printf("six\n");
  else if(a == 7) printf("seven\n");
  else if(a == 8) printf("eight\n");
  else if(a == 9) printf("nine\n");
}


int main(int argc, char const *argv[]) {

  int s, d;
  scanf("%d", &s);

  if(s == 0) printf("zero\n");
  else if(s < 10) print_num(s);
  else if(s == 10) printf("ten\n");
  else if(s == 11) printf("eleven\n");
  else if(s == 12) printf("twelve\n");
  else if(s == 13) printf("thirteen\n");
  else if(s == 14) printf("fourteen\n");
  else if(s == 15) printf("fifteen\n");
  else if(s == 16) printf("sixteen\n");
  else if(s == 17) printf("seventeen\n");
  else if(s == 18) printf("eighteen\n");
  else if(s == 19) printf("nineteen\n");
  else if(s >= 20) {
    d = s/10;
    if(d == 2) printf("twenty");
    else if(d == 3) printf("thirty");
    else if(d == 4) printf("forty");
    else if(d == 5) printf("fifty");
    else if(d == 6) printf("sixty");
    else if(d == 7) printf("seventy");
    else if(d == 8) printf("eighty");
    else if(d == 9) printf("ninety");
    if(s%10) {
      printf("-");
      print_num(s%10);
    }
    else if(s%10 == 0) printf("\n");
  }

  return 0;
}
