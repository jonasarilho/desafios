#include "bits/stdc++.h"

using namespace std;

typedef long long ll;
typedef pair<ll, ll> pll;

#define MAX 200005

int main(int argc, char const *argv[]) {

    int n, m;
    scanf("%d %d", &n, &m);

    vector<pll> G[n+1];
    ll dist[n+1];
    priority_queue< pll, vector<pll>, greater<pll> > PQ;

    for (int i = 0; i < m; i++) {
        int u, v;
        ll w;
        scanf("%d %d %lld", &u, &v, &w);
        G[u].push_back(make_pair(v, 2*w));
        G[v].push_back(make_pair(u, 2*w));
    }

    for (ll i = 1; i <= n; i++) {
        scanf("%lld", &dist[i]);
        PQ.push(make_pair(dist[i],i));
    }

    while (!PQ.empty()) {
        pll s = PQ.top();
        ll t = s.first;
        ll u = s.second;
        PQ.pop();

        if(dist[u] == t) {
            for (auto node : G[u]) {
                ll v = node.first;
                ll w = node.second;

                if(dist[v] > dist[u] + w) {
                    dist[v] = dist[u] + w;
                    PQ.push(make_pair(dist[v], v));
                }
            }
        }
    }

    for (int i = 1; i <= n; i++) printf("%lld ", dist[i]);
    printf("\n");


    return 0;
}
