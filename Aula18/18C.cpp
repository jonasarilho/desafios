#include "bits/stdc++.h"

using namespace std;
#define FILE_IN freopen("h.in", "r", stdin);

int main(int argc, char const *argv[]) {

    FILE_IN

    int m, n;

    scanf("%d", &m);
    scanf("%d", &n);

    int sum = m + 1;
    for (int x = 1; x <= m; x++) {
        for (int y = x; y <= m; y++) {
            for (int z = y; z <= m; z++) {
                if(x * x + y * y == z * z) sum++;
            }
        }
    }
    sum += n - 2;
    for (int i = 3; i <= n; i++) sum += m;
    printf("%d\n", sum);

    return 0;
}
