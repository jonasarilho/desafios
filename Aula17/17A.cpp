#include "bits/stdc++.h"

using namespace std;

void task() {

    int n;
    scanf("%d", &n);

    double maxT = 0;
    int ans = 0;
    for (int i = 1; i <= n; i++) {
        double a, b, c;
        scanf("%lf %lf %lf", &a, &b, &c);

        double R = b / (2 * a);
        double T = (-a * R * R) + (b * R) + c;
        if(T > maxT) {
            ans = i;
            maxT = T;
        }
    }
    printf("%d\n", ans);

}


int main(int argc, char const *argv[]) {

    int t;

    scanf("%d", &t);

    for (int i = 0; i < t; i++) {
        task();
    }

    return 0;
}
