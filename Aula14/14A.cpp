#include "bits/stdc++.h"

using namespace std;

const int MAXN = 3601;

void task() {

    int n, t;
    scanf("%d %d", &n, &t);
    vector<int> b(n);
    for (int i = 0; i < n; i++) scanf("%d", &b[i]);

    queue<int> Q;
    Q.push(0);
    vector<int> v(MAXN);
    int level = 0, buttons = -1, tempo = 1e9;
    while (!Q.empty()) {
        int m = Q.size();
        while (m--) {
            int val = Q.front();
            Q.pop();
            if(val >= t && val < tempo) {
                tempo = val;
                buttons = level;
            }
            for (int i = 0; i < n; i++) {
                int next = max(0, b[i] + val);
                next = min(next, 3600);
                if(v[next] == 0) {
                    v[next] = 1;
                    Q.push(next);
                }
            }
        }
        level++;
    }

    printf("%d %d\n", buttons, tempo - t);

}


int main(int argc, char const *argv[]) {

    int t;

    scanf("%d", &t);

    for (int i = 0; i < t; i++) {
        task();
    }

    return 0;
}
