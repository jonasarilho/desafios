#include "bits/stdc++.h"

using namespace std;

#define MAX 60
#define FILE_IN freopen("jealous.in", "r", stdin);
#define FILE_OUT freopen("jealous.out", "w", stdout);

typedef unsigned long long ll;

int main(int argc, char const *argv[]) {

    FILE_IN
    FILE_OUT

    ll a, b, p, q;
    vector<ll> p_array(MAX + 1), q_array(MAX + 1);

    scanf("%llu %llu %llu %llu", &a, &b, &p, &q);

    p_array[0] = q_array[0] = 1;
    ll px, qx, y;
    px = qx = 0;
    y = b;
    while (y / p) {
        px++;
        y /= p;
    }
    y = b;
    while (y / q) {
        qx++;
        y /= q;
    }

    for (int i = 0; i < MAX; i++) {
        p_array[i+1] = p_array[i] * p;
        q_array[i+1] = q_array[i] * q;
        if(px < i + 1) p_array[i+1] = 0;
        if(qx < i + 1) q_array[i+1] = 0;
    }

    ll z[4], ans = 0LL;

    for(int i = 1; p_array[i] <= b && p_array[i] > 0; i++){
        for (int j = 0; j < 4; j++) z[j] = 0;

        z[0] = b / p_array[i];
        z[1] = z[0] / p;
        if(i < q_array.size() && q_array[i] > 0) z[2] = z[0] / q_array[i];
        if(q_array[i] * p > 0) z[3] = z[0] /(q_array[i] * p);
        ans += (z[0] - z[1] - z[2] + z[3]);


        z[0] = (a-1) / p_array[i];
        z[1] = z[0] / p;
        if(i < q_array.size() && q_array[i] > 0) z[2] = z[0] / q_array[i];
        if(q_array[i] * p > 0) z[3] = z[0] /(q_array[i] * p);
        ans -= (z[0] - z[1] - z[2] + z[3]);
    }

    printf("%llu\n", ans);


    return 0;
}
