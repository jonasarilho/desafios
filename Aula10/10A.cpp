#include "bits/stdc++.h"

using namespace std;

#define MAX ULLONG_MAX
typedef unsigned long long ll;


ll power(ll n, ll i) {
    long long j = 0LL, p = i, lim = n / i;

    while (true) {
        j += n / p;
        if (p > lim) break;
        p *= i;
    }

    return j;
}

void task() {
    ll n, k;
    scanf("%lld %lld", &n, &k);

    ll lim = sqrt(k+1LL), ans =  MAX;

    if(k % 2 == 0LL) {
        ll j = 0LL;
        while (k % 2 == 0LL)  {
            j++;
            k /= 2;
        }
        ans = min(ans, power(n,2) / j);
    }
    for (ll i = 3LL; i <= lim; i+=2LL) {
        if(k % i == 0LL) {
            ll j = 0LL;
            while (k % i == 0LL)  {
                j++;
                k /= i;
            }
            ans = min(ans, power(n,i) / j);
        }
    }
    if (k > 1LL) ans = min(ans, power(n,k));
    printf("%lld\n", ans);

}


int main(int argc, char const *argv[]) {

    int t;

    scanf("%d", &t);

    for (int i = 0; i < t; i++) {
        task();
    }

    return 0;
}
