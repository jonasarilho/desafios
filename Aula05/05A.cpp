#include "bits/stdc++.h"

using namespace std;

long long position(char *x) {
    int len = strlen(x);
    if (len == 1) {
        if (x[0] == '0') return 0;
        else return 1;
    }
    if (x[0] == '0') return position(x+1);
    else {
        long long res = 1;
        for (int i = 0; i < len; i++) res *= 2;
        return res - position(x+1) - 1;
    }
    return 1;
}

int main(int argc, char const *argv[]) {

    int n;
    scanf("%d", &n);
    char a[n+1], b[n+1];
    scanf("%s %s", a, b);

    printf("%lld\n", position(b) - position(a) - 1);

    return 0;
}
