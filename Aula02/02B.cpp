#include <bits/stdc++.h>

#define N 1000000
#define MODULO 1000000007

using namespace std;

char a[N];
int b[N], c[N], y[N];

int main(int argc, char const *argv[]) {

    int n, m;
    scanf("%d %d", &n, &m);
    scanf("%s", a);
    int len = strlen(a);
    for (int i = 0; i < m; i++) {
        scanf("%d", &y[i]);
        y[i]--;
    }

    int p = -1; b[0] = -1;
    for (int i = 1; i < len; i++) {
		while (p != -1 && a[p + 1] != a[i]) p = b[p];
		if (a[p + 1] == a[i]) ++p;
		b[i] = p;
	}

	int k = len - 1;
	while (b[k] != -1) {
		c[b[k]]++;
		k = b[k];
	}

    long long ans = 1;
    int r = n - len;

	for (int i = 1; i < m; i++) {
        int dif = y[i] - y[i - 1];
		if (dif < len) {
			if (c[len - dif - 1]) r -= dif;
			else ans = 0;
		}
		else r -= len;
	}

    if(!m) r = n;
    for (int i = 0; i < r; i++) {
        ans = (ans * 26) % MODULO;
    }
    printf("%d\n", (int)ans);


    return 0;
}
