#include "bits/stdc++.h"

using namespace std;

#define MAX 100001

void task(){
    int n, h;
    stack<int> p;

    scanf("%d %d",&n,&h);
    vector<int> v[MAX];
    int ans[MAX],clue[MAX];

    for (int i = 0; i < MAX; i++) ans[i] = clue[i] = 0;

    for (int i = 0; i < h; i++) {
        int a, b;
        scanf("%d %d", &a, &b);
        v[a].push_back(b);
        clue[b]++;
    }
    for(int i = 1; i <= n; i ++) if(clue[i] == 0) p.push(i);

    int sum = 0, miss = 0;
    while(!p.empty()) {
        if(p.size() > 1) miss = 1;
        int u = p.top();
        p.pop();
        ans[sum++] = u;
        for(int i = 0; i < v[u].size(); i ++) {
            int w = v[u][i];
            clue[w]--;
            if(clue[w] == 0) p.push(w);
        }
    }
    if (sum < n) printf("recheck hints\n");
    else if (miss == 1) printf("missing hints\n");
    else {
        for(int i = 0; i < n; i ++) {
				printf("%d",ans[i]);
				if(i == n - 1) printf("\n");
				else printf(" ");
			}
    }

}


int main(int argc, char const *argv[]) {

    int t;

    scanf("%d", &t);

    for (int i = 0; i < t; i++) {
        task();
    }

    return 0;
}
