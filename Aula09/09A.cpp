#include "bits/stdc++.h"

using namespace std;

#define MAX 100003
#define FILE_IN freopen("homo.in", "r", stdin);
#define FILE_OUT freopen("homo.out", "w", stdout);

int eq, dif, num_count[MAX], num_id;
map<int,int> dic;

void insert_num(int k) {
    if(num_count[k] > 1) eq--;
    else if(num_count[k] == 0) dif++;
    num_count[k]++;
    if(num_count[k] > 1) eq++;
    return;
}

void delete_num(int k) {
    if(num_count[k] == 0) return;
    if(num_count[k] > 1) eq--;
    else if(num_count[k] == 1) dif--;
    num_count[k]--;
    if(num_count[k] > 1) eq++;
    return;
}

int id(int k) {
    int i;
    if (dic.count(k)) i = dic[k];
    else {
        i = num_id++;
        dic[k] = i;
    }
    return i;
}

int main(int argc, char const *argv[]) {

    FILE_IN
    FILE_OUT

    int n;
    scanf("%d", &n);

    eq = dif = num_id = 0;
    for (int i = 0; i < n; i++) {
        char s[10];
        int k;
        scanf("%s", s);
        scanf("%d", &k);

        k = id(k);
        if (strcmp(s, "insert") == 0) insert_num(k);
        else delete_num(k);

        if (eq > 0 && dif > 1) printf("both\n");
        else if (eq > 0) printf("homo\n");
        else if (dif > 1) printf("hetero\n");
        else printf("neither\n");
    }

    return 0;
}
