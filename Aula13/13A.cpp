#include "bits/stdc++.h"

using namespace std;

const int XMAX = 75;
const int YMAX = 100;

int good_job(double w, int n, int max) {
    vector<double> field(n);
    double half = w / 2;
    int ans = 0;
    for (int i = 0; i < n; i++) scanf("%lf", &field[i]);
    sort(field.begin(), field.end());

    if ((field[0] <= half) && (((double)max - field[n-1]) <= half)) ans = 1;
    for (int i = 0; i < n-1; i++) if(field[i+1] - field[i] > w) return 0;

    return ans;
}

int main(int argc, char const *argv[]) {

    int nx, ny;
    double w;
    scanf("%d %d %lf", &nx, &ny, &w);
    int ok = 1;
    while (ok) {

        int x, y;
        x = good_job(w, nx, XMAX);
        y = good_job(w, ny, YMAX);

        if(x && y) printf("YES\n");
        else printf("NO\n");

        scanf("%d %d %lf", &nx, &ny, &w);
        if(nx == 0 && ny == 0 && w == 0.0) ok =  0;
    }

    return 0;
}
