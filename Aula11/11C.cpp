#include "bits/stdc++.h"

using namespace std;

#define SIZE 5
#define MAX 12
#define MAGIC 26

char board[SIZE][10];
const int points[12][2] = {
    {0, 4}, {1, 1}, {1, 3}, {1, 5},
    {1, 7}, {2, 2}, {2, 6}, {3, 1},
    {3, 3}, {3, 5}, {3, 7}, {4, 4}
};
const int lines[6][4] = {
	{0, 2, 5, 7},
	{1, 2, 3, 4},
	{0, 3, 6, 10},
	{7, 8, 9, 10},
	{1, 5, 8, 11},
	{4, 6, 9, 11}
};
int visited[MAX];

int ok() {
	for (int i = 0; i < 6; i++) {
		int x = 0, sum = 0;
		for (int j = 0; j < 4; j++) {
			int n = lines[i][j], a = points[n][0], b = points[n][1];
			if (board[a][b] == 'x') x++;
			else sum += board[a][b] - 'A' + 1;
		}
		if ((sum > MAGIC) ||
            (sum == MAGIC && x > 0) ||
            (x == 0 && sum != MAGIC)) return 0;
	}
	return 1;
}

int dfs(int n) {
	if (n == MAX) return 1;
	int a = points[n][0], b = points[n][1];
	if (board[a][b] != 'x') return dfs(n + 1);

	for (int i = 0; i < MAX; i++) {
		if (visited[i]) continue;
		visited[i] = 1;
		board[a][b] = 'A' + i;
		if (ok() && dfs(n + 1)) return 1;
		board[a][b] = 'x';
		visited[i] = 0;
	}
	return 0;
}


int main(int argc, char const *argv[]) {

    for (int i = 0; i < SIZE; i++) scanf("%s", board[i]);
    for (int i = 0; i < MAX; i++) visited[i] = 0;
    for (int i = 0; i < MAX; i++) {
        int a = points[i][0], b = points[i][1];
    	if (board[a][b] != 'x') {
            visited[board[a][b] - 'A'] = 1;
        }
    }
    dfs(0);
    for (int i = 0; i < SIZE; i++) printf("%s\n", board[i]);


    return 0;
}
