#include "bits/stdc++.h"

using namespace std;

#define MAX 1000000000

int main(int argc, char const *argv[]) {

    int t, n, b;
    scanf("%d", &t);

    while (t--) {
        scanf("%d %d", &n, &b);
        int num_types = 0;
        map<string, int> type_dic;
        int prices[n][n], qualities[n][n], np = 0, nq = 0;
        for (size_t i = 0; i < n; i++) {
            for (size_t j = 0; j < n; j++) {
                prices[i][j] = INT_MAX;
                qualities[i][j] = 0;
            }
        }
        for (size_t i = 0; i < n; i++) {
            string type, name;
            int price, quality;
            cin >> type >> name;
            scanf("%d %d", &price, &quality);
            if(!type_dic.count(type)) type_dic[type] = num_types++;
            int j = type_dic[type];
            prices[j][np++] = price;
            qualities[j][nq++] = quality;
        }

        int left = 0, right = MAX;
        while (left < right) {
            int budget = b, mid = (left + right + 1) / 2;
            for (size_t i = 0; i < num_types; i++) {
                int best = INT_MAX;
                for (size_t j = 0; j < np; j++) {
                    if(qualities[i][j] >= mid) {
                        best = min(best, prices[i][j]);
                    }
                }
                budget -= best;
                if(budget < 0) {
                    right = mid - 1;
                    break;
                }
            }
            if (budget >= 0) left = mid;
            else right = mid - 1;
        }

        printf("%d\n", left);
    }

    return 0;
}
