#include <bits/stdc++.h>

char *notprime;

void calculates_primes(int n) {

    notprime = (char*)calloc(n + 1, sizeof(char));

    notprime[0] = 1;
    notprime[1] = 1;

    if(n == 1) return;
    if(n == 2) return;

    notprime[2] = 0;
    int lim = sqrt(n+1);
    for (int j = 2; 2 * j <= lim; j++) notprime[2*j] = 1;
    if(n % 2 == 0) notprime[n] = 1;
    for (int i = 3; i <= lim; i+=2) {
        if(notprime[i] == 0) {
            if(n % i == 0) notprime[n] = 1;
            for (int j = 2; j * i < lim; j++) notprime[j*i] = 1;
        }
    }
}


int main(int argc, char const *argv[]) {

  int n;
  scanf("%d", &n);

  calculates_primes(n);

  int lim = sqrt(n+1);
  if(notprime[n] == 0) {
      printf("yes\n");
      return 0;
  }
  for (int i = 2; i <= lim; i++) {
      if(notprime[i] == 0) {
          long long j;
          for (j = i; j < n; j*=i);
          if(j == n) {
              printf("yes\n");
              return 0;
          }
      }
  }

  printf("no\n");

  free(notprime);

  return 0;
}
