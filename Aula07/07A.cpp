#include "bits/stdc++.h"

using namespace std;

int main(int argc, char const *argv[]) {

    int n;
    scanf("%d", &n);

    int h[n+1], c[n+1];
    for (int i = 0; i <= n; i++) {
        h[i] = 0;
        c[i] = 0;
    }
    for (int i = 2; i <= n; i++) {
        int inf;
        scanf("%d", &inf);
        h[i] = h[inf] + 1;
        c[h[i]]++;
    }

    int res = 1;
    for (int i = 1; i <= n; i++) res += (c[i] % 2);

    printf("%d\n", res);

    return 0;
}
