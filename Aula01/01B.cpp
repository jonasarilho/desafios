#include <stdio.h>
#include <stdlib.h>

#define MIN(a, b) (a < b ? a : b)

long long A, B;

long long get_result(long long l, long long t, long long m) {

  long long left = 1, right = 10000000, mid, max;
  max = t * m * 2;

  while (left <= right) {
      mid = (left + right)/ 2;
      if((mid - l + 1) * (2 * A + (l + mid -2) * B) <= max) {
          left = mid + 1;
      }
      else right = mid - 1;
  }

  return MIN((t - A + B) / B, right);
}


int main(int argc, char const *argv[]) {

  long long n;
  scanf("%lld %lld %lld", &A, &B, &n);

  long long R[100000];

  for (int i = 0; i < n; i++) {
    long long l, t, m;
    scanf("%lld %lld %lld", &l, &t, &m);
    if (t < A + (l-1)* B) R[i] = -1;
    else R[i] = get_result(l, t, m);
  }

  for (int i = 0; i < n; i++) {
    printf("%lld\n", R[i]);
  }

  return 0;
}
