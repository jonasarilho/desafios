#include "bits/stdc++.h"

using namespace std;

const int MAXN = 100001;
const int MAX = INT_MAX;

vector<int> a, b, c;
int lin_tree[MAXN * 4];

bool func(int i, int j) {
    if (a[i] != a[j]) return a[i] < a[j];
    if (b[i] != b[j]) return b[i] < b[j];
    return c[i] < c[j];
}

void rmq_create(int i, int l, int r, int left, int right) {
    if (l > left || r < left) return;
    if (l == left && r == left) {
        lin_tree[i] = min(lin_tree[i], right);
        return;
    }
    int mid = l + (r - l) / 2;
    rmq_create(i * 2, l, mid, left, right);
    rmq_create(i * 2 + 1, mid + 1, r, left, right);
    lin_tree[i] = min(lin_tree[i * 2], lin_tree[i * 2 + 1]);
}

int rmq_value(int i, int l, int r, int left, int right) {
    if (l > right || r < left) return MAX;
    if (l >= left && r <= right) return lin_tree[i];

    int mid = l + (r - l) / 2;
    int l_val, r_val;
    l_val = rmq_value(i * 2, l, mid, left, right);
    r_val = rmq_value(i * 2 + 1, mid + 1, r, left, right);
    return min(l_val, r_val);
}

void task() {

    int n;
    scanf("%d", &n);
    for (int i = 0; i < n; i++) {
        int r1, r2, r3;
        scanf("%d %d %d", &r1, &r2, &r3);
        a.push_back(r1);
        b.push_back(r2);
        c.push_back(r3);
    }

    vector<int> id(n);
    for (int i = 0; i < n; i++) id[i] = i;
    sort(id.begin(), id.end(), func);

    fill(lin_tree, lin_tree + MAXN * 4, MAX);
    int j = 0, ans = 0;
    for (int k = 0; k < n; k++) {
        int i = id[k];

        while (a[id[j]] < a[i]) {
            rmq_create(1, 0, n, b[id[j]], c[id[j]]);
            j++;
        }

        if (rmq_value(1, 0, n, 0, b[i] - 1) >= c[i])
            ans++;
    }
    printf("%d\n", ans);
    a.clear();
    b.clear();
    c.clear();
}


int main(int argc, char const *argv[]) {

    int t;

    scanf("%d", &t);

    for (int i = 0; i < t; i++) {
        task();
    }

    return 0;
}
