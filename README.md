# Desafios

Este repositório é para exercícios de programação competitiva.

## Aula 01

[A - Tavas and Nafas](http://codeforces.com/problemset/problem/535/A)

[B - Tavas and Karafs](http://codeforces.com/problemset/problem/535/C)

## Aula 02

[A - Tavas and SaDDas](http://codeforces.com/problemset/problem/535/B)

[B - Tavas and Malekas](http://codeforces.com/problemset/problem/536/B)

## Aula 03

[A - Older Brother](https://open.kattis.com/problems/olderbrother)

[B - Chef and Mover](https://www.codechef.com/problems/CHEFMOVR)

## Aula 04

[A - Greedy Candidates](https://www.codechef.com/problems/GCAC)

[B - Exam Redistribution](http://codeforces.com/gym/101170/attachments)

## Aula 05

[A - Hamiltonian Hypercube](http://codeforces.com/gym/101170/attachments)

[B - Buy a Ticket](http://codeforces.com/problemset/problem/938/D)

## Aula 06

[A - Boss Battle](http://codeforces.com/gym/101623/attachments)

[B - High Score](http://codeforces.com/gym/101623/attachments)
