#include "bits/stdc++.h"

using namespace std;

typedef long long ll;

const int MAXN = 20001;

int main(int argc, char const *argv[]) {

    int n;
    scanf("%d", &n);

    vector<ll> v(n);
    for (int i = 0; i < n; i++) scanf("%lld", &v[i]);

    sort(v.begin(), v.end());
    bool ok = false;
    for (int i = 2; i < n; i++) {
        if(v[i] < v[i-1] + v[i-2]) {
            ok = true;
            break;
        }
    }
    if(ok) printf("possible\n");
    else printf("impossible\n");


    return 0;
}
