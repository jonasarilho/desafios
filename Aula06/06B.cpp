#include "bits/stdc++.h"
typedef long long ll;
#define MAX 1000000000

using namespace std;

ll score(ll a, ll b, ll c) {

    return a * a + b * b + c * c + 7 * (min(min(a,b), c));
}

int main(int argc, char const *argv[]) {

    int n;
    scanf("%d", &n);

    for (ll i = 0; i < n; i++) {
        ll a, b, c, d;
        scanf("%lld %lld %lld %lld", &a, &b, &c, &d);
        if (d == 0LL) printf("%lld\n", score(a, b ,c));
        else {
            ll lim = min(d, 1000LL);
            ll sc = 0;
            for (ll j = 0; j <= lim; j++) {
                for (ll k = 0; j + k <= lim; k++) {
                    ll dif = d - j - k;
                    sc = max(sc, score(a+j, b+k, c+dif));
                    sc = max(sc, score(a+j, b+dif, c+k));
                    sc = max(sc, score(a+dif, b+j, c+k));
                }
            }
            sc = max(sc, score(a+d, b, c));
            sc = max(sc, score(a, b+d, c));
            sc = max(sc, score(a, b, c+d));

            printf("%lld\n", sc);
        }
    }

    return 0;
}
