#include "bits/stdc++.h"

using namespace std;

int main(int argc, char const *argv[]) {

    int n;
    while (scanf("%d", &n) != EOF) {
        std::vector< pair<int,int> > students;
        for (int i = 0; i < n; i++) {
            int s;
            scanf("%d", &s);
            students.push_back(make_pair(s, i+1));
        }
        sort(students.begin(), students.end());

        int sum = 0;
        for (int i = 0; i < n - 1; i++) sum += students[i].first;

        if (sum < students[n-1].first) printf("impossible");

        else for (int i = 0; i < n; i++) printf("%d ", students[n-i-1].second);

        printf("\n");
    }


    return 0;
}
