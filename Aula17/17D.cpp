#include "bits/stdc++.h"

using namespace std;

int main(int argc, char const *argv[]) {

    int n;
    scanf("%d", &n);
    int a[100000], b[100000];

    for (int i = 0; i <= 255; i++) {
        int p;
        p = (i << 1) & 255;
        int q = (i) ^ (p);
        a[q] = i;
    }

    for (int i = 0; i < n; i++) scanf("%d", &b[i]);

    for (int i = 0; i < n; i++) printf("%d ", a[b[i]]);
    printf("\n");

    return 0;
}
