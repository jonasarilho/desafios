#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char const *argv[]) {

    char n[10];
    int id = 1;
    scanf("%s", n);

    for (int i = 0; n[i]; i++) {
        id *= 2;
        if(n[i] == '7') id++;
    }
    id -= 1;

    printf("%d\n", id);


    return 0;
}
