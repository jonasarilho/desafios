#include "bits/stdc++.h"

using namespace std;

#define MAX 1000000001

int main(int argc, char const *argv[]) {

    int n;
    scanf("%d", &n);

    int a[n];
    for (int i = 0; i < n; i++) scanf("%d", &a[i]);

    map<int ,int> dic;
    int ans = 0;
    for (int i = 0; i < n; i++) {
        dic[a[i]]++;
        ans = max(ans, dic[a[i]]);
    }

    printf("%d\n", ans);

    return 0;
}
