#include "bits/stdc++.h"

using namespace std;

#define MAX 100000
#define FILE_IN freopen("bureau.in", "r", stdin);
#define FILE_OUT freopen("bureau.out", "w", stdout);

int main(int argc, char const *argv[]) {

    FILE_IN
    FILE_OUT

    int n;
    scanf("%d", &n);

    int law[n], active[n];
    for (int i = 0; i < n; i++) {
        char s[10];
        scanf("%s", s);
        active[i] = 1;
        if (strcmp(s, "declare") == 0) law[i] = 0;
        else {
            int l;
            scanf("%d", &l);
            law[i] = l;
        }
    }

    for (int i = n - 1; i >= 0; i--)
        if (active[i] && law[i])
            active[law[i]-1] = 0;

    int sum = 0;
    for (int i = 0; i < n; i++) if (active[i]) sum++;

    printf("%d\n", sum);

    for (int i = 0; i < n; i++) if (active[i]) printf("%d ", i+1);;
    printf("\n");

    return 0;
}
