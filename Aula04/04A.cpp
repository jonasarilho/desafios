#include "bits/stdc++.h"

using namespace std;

void task() {

    int n, m;
    scanf("%d %d", &n, &m);

    int minSalary[n], offeredSalary[m], maxJobOffers[m];
    for (int i = 0; i < n; i++) {
        scanf("%d", &minSalary[i]);
    }
    for (int i = 0; i < m; i++) {
        scanf("%d %d", &offeredSalary[i], &maxJobOffers[i]);
    }

    char qual[n][m+2];
    for (int i = 0; i < n; i++) {
        scanf("%s", qual[i]);
    }

    int jobs = 0, companies[m];
    long long salary = 0;
    for (int i = 0; i < m; i++) companies[i] = 0;

    for (int i = 0; i < n; i++) {
        int best = -1;
        for (int j = 0; j < m; j++) {
            if(best == -1) {
                if(qual[i][j] == '1' && maxJobOffers[j] > 0 && offeredSalary[j] >= minSalary[i])
                    best = j;
            }
            else if(qual[i][j] == '1' && maxJobOffers[j] > 0
                && offeredSalary[j] > offeredSalary[best]
                && offeredSalary[j] >= minSalary[i])
                    best = j;
        }
        if(best != -1) {
            maxJobOffers[best]--;
            jobs++;
            salary+= offeredSalary[best];
            companies[best] = 1;
        }
    }
    int no_employees = 0;
    for (int i = 0; i < m; i++) if(companies[i] == 0) no_employees++;

    printf("%d %lld %d\n", jobs, salary, no_employees);

}

int main(int argc, char const *argv[]) {

    int t;

    scanf("%d", &t);

    for (int i = 0; i < t; i++) {
        task();
    }

    return 0;
}
