#include "bits/stdc++.h"

using namespace std;

const int combo = (int)'R' + (int)'B' + (int)'L';

int main(int argc, char const *argv[]) {

    string s, t;
    cin >> s;

    t = "";

    for (int i = 0; i < s.length(); i++) {
        if((i < s.length() - 2) and ((int)s[i] + (int)s[i+1] + (int)s[i+2] == combo)) {
            t.append("C");
            i+= 2;
        }
        else if(s[i] == 'R') t.append("S");
        else if(s[i] == 'B') t.append("K");
        else if(s[i] == 'L') t.append("H");
    }


    cout << t << endl;

    return 0;
}
