#include "bits/stdc++.h"

using namespace std;

int k, n;
vector<string> s;
string ans;

int valid_answer() {

    for (int i = 0; i < k; i++) {
        set<char> dic;
        vector<int> v;
        for (int j = 0; j < n; j++) {
            dic.insert(s[i][j]);
            if(s[i][j] != ans[j]) v.push_back(j);
        }

        if(v.empty()) {
             if(dic.size() >= n) return 0;
        }
        else {
             if(v.size() != 2) return 0;
             if(ans[v[1]] != s[i][v[0]] || ans[v[0]] != s[i][v[1]]) return 0;
        }
    }
    return 1;

}

int valid_swap(int a, int b) {
    vector<int> dif;
    for (int i = 0; i < n; i++) {
        if(s[a][i] != s[b][i]) dif.push_back(i);
    }
    if(dif.size() > 4) return 0;

    ans = s[a];
    if(valid_answer()) return 1;

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < dif.size(); j++) {
            ans = s[a];
            swap(ans[i], ans[dif[j]]);
            if(valid_answer()) return 1;
        }
    }
    return 0;
}



int main(int argc, char const *argv[]) {

    scanf("%d %d", &k, &n);

    for (int i = 0; i < k; i++) {
        string a;
        cin >> a;
        s.push_back(a);
    }

    int same = 1;

    for (int i = 0; i < k; i++) {
        for (int j = i+1; j < k; j++) {
            if(s[i] != s[j]) {
                if(valid_swap(i,j) == 0) printf("-1\n");
                else cout << ans << endl;
                same = 0;
                i = j = k;
            }
        }
    }
    if(same) {
        ans = s[0];
        swap(ans[0], ans[1]);
        cout << ans << endl;
    }


    return 0;
}
