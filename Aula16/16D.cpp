#include "bits/stdc++.h"

using namespace std;

void task() {

    string s, t;
    cin >> s;
    t = string(110, '#');
    vector<string> maze(300, t);
    int x = 150, y = 0;
    maze[x][y] = '.';

    const int next[] = {0,1,0,-1};
    int pos = 0;
    for (int i = 0; i < s.length(); i++) {
        if (s[i] == 'R')
            pos = (pos + 1) % 4;
        else if (s[i] == 'L')
            pos = (pos - 1 + 4) % 4;
        else if (s[i] == 'B')
            pos = (pos + 2) % 4;
        x += next[pos];
        y += next[(pos + 1) % 4];
        maze[x][y] = '.';
    }

    int w = 0;
    vector<string> ans;
    for (int i = 0; i < maze.size(); i++) {
        if (find(maze[i].begin(), maze[i].end(), '.') != maze[i].end()) {
            ans.push_back(maze[i]);
            int cw = ans.back().size() - 1;
            while (ans.back()[cw] == '#') cw--;
            w = max(w, cw);
        }
    }
    w += 2;

    printf("%d %d\n", (int)ans.size() + 2, w);
    for (int i = 0; i < w; i++) printf("#");
    printf("\n");
    for (int i = 0; i < ans.size(); i++) {
        cout << ans[i].substr(0, w) << endl;
    }
    for (int i = 0; i < w; i++) printf("#");
    printf("\n");


}


int main(int argc, char const *argv[]) {

    int t;

    scanf("%d", &t);
    printf("%d\n", t);

    for (int i = 0; i < t; i++) {
        task();
    }

    return 0;
}
