#include "bits/stdc++.h"

using namespace std;

#define MAX 100001

int n;
vector<int> *G;

int bfs1(int x) {
    int u, visited[n+1];
    for (int i = 0; i < n + 1; i++) visited[i] = 0;
    queue<int> Q;
    Q.push(x);
    while (!Q.empty()) {
        u = Q.front();
        Q.pop();
        visited[u] = 1;
        for (int i = 0; i < G[u].size(); i++) {
            int v = G[u][i];
            if(!visited[v]) Q.push(v);
        }
    }
    return u;
}

int bfs2(int x) {
    int u, visited[n+1], dist[n+1];
    for (int i = 0; i < n + 1; i++) visited[i] = 0;
    queue<int> Q;
    Q.push(x);
    dist[x] = 0;
    while (!Q.empty()) {
        u = Q.front();
        Q.pop();
        visited[u] = 1;
        for (int i = 0; i < G[u].size(); i++) {
            int v = G[u][i];
            if(!visited[v]) {
                dist[v] = dist[u] + 1;
                Q.push(v);
            }
        }
    }
    return (dist[u] + 1) / 2;

}


void task() {

    scanf("%d", &n);
    G = new vector<int>[n+1];

    int used[n+1];
    for (int i = 0; i < n + 1; i++) used[i] = 0;
    for (int i = 0; i < n - 1; i++) {
        int a, b;
        scanf("%d %d", &a, &b);
        used[b]++;
        G[a].push_back(b);
        G[b].push_back(a);
    }

    int last;
    for (last = 0; last < n && used[last]; last++);

    int u, d;
    u = bfs1(last);
    d = bfs2(u);
    printf("%d\n", d);

}


int main(int argc, char const *argv[]) {

    int t;

    scanf("%d", &t);

    for (int i = 0; i < t; i++) {
        task();
    }

    return 0;
}
