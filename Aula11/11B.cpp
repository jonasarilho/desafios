#include "bits/stdc++.h"

using namespace std;

int GRT(char *a, char *b, int n) {
    int mid = (n + 1) / 2;
    for (int i = 0; i < n; i++) {
    	int k = 0;
    	for (int j = 0; i + j < n; j++) {
    		if (abs(a[j] - b[i + j]) <= 1) k++;
    		else k = 0;
    		if (k >= mid) return 1;
    	}
    }
    return 0;
}


void task() {

    int n;
    scanf("%d", &n);

    char a[n+1], b[n+1];
    scanf("%s %s", a, b);

    if(GRT(a, b, n) || GRT(b, a, n)) printf("POSITIVE\n");
    else printf("NEGATIVE\n");

}


int main(int argc, char const *argv[]) {

    int t;

    scanf("%d", &t);

    for (int i = 0; i < t; i++) {
        task();
    }

    return 0;
}
