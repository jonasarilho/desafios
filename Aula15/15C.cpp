#include "bits/stdc++.h"

using namespace std;

const int MAXN = 1024;
const int MAXX = 9001;


int main(int argc, char const *argv[]) {

    int n;
    scanf("%d", &n);

    vector<int> a(n);
    bool ok = true;
    scanf("%d", &a[0]);
    for (int i = 1; i < n; i++) {
        scanf("%d", &a[i]);
        if(a[i] < a[i-1]) ok = false;
    }

    if(ok) printf("yes\n");
    else printf("no\n");

    return 0;
}
