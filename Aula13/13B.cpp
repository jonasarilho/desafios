#include "bits/stdc++.h"

using namespace std;

const int VMAX = 1500;

struct NODE {
    int a, b, l;
};
struct DFS {
    vector<NODE> prev;
    vector<int> depth, dfsin, dfsout;
};
struct KEYS {
    vector<int> key;
    vector<bool> found, needed;
};

typedef vector<vector<NODE> > graph;

int turn;

int lca(graph &G, int a, int b, DFS &s) {

    if (s.depth[a] > s.depth[b]) swap(a, b);
    while (!(s.dfsin[a] <= s.dfsin[b] && s.dfsout[a] >= s.dfsout[b]))
        a = s.prev[a].a;
    return a;
}

void best_path(int a, int l, DFS &s, list<NODE> &ret) {

    while (a != l) {
        NODE anc = s.prev[a];
        swap(anc.a, anc.b);
        ret.push_back(anc);
        a = anc.b;
    }
}

void dfs(graph &G, int x, int d, DFS &s) {
  s.depth[x] = d;
  s.dfsin[x] = turn++;
  for (int i = 0; i < G[x].size(); i++) {
      int y = G[x][i].b;
      if (s.depth[y] < 0) {
          s.prev[y] = G[x][i];
          dfs(G, y, d + 1, s);
      }
  }

  s.dfsout[x] = turn;
}

bool exists_path(graph &G, int a, int b, list<NODE> &ret, DFS &s, KEYS &k) {

    if (a != b) {
        list<NODE> ret2;
        int l = lca(G, a, b, s);
        best_path(a, l, s, ret2);
        best_path(b, l, s, ret);
        ret.reverse();
        for (list<NODE>::iterator it = ret.begin(); it != ret.end(); it++) {
            swap(it->a, it->b);
        }
        ret.splice(ret.begin(), ret2);
    }
    list<NODE>::iterator prev = ret.begin();
    for (list<NODE>::iterator edge = ret.begin(); edge != ret.end();) {
        int m = edge->a, p = k.key[edge->l];
        if (edge->l >= 0 && !k.found[edge->l]) {

            if (k.needed[edge->l]) return false;
            k.needed[edge->l] = true;
            list<NODE> first, second;
            bool path1, path2;
            path1 = exists_path(G, prev->a, p, first, s, k);
            if(!path1) return false;
            path2 = exists_path(G, p, m, second, s, k);
            if(!path2) return false;
            k.found[edge->l] = true;

            second.splice(second.begin(), first);
            ret.erase(prev, edge);
            ret.splice(edge, second);
            prev = ++edge;
        } else edge++;
    }

    return true;
}

int main(int argc, char const *argv[]) {

    int v, c, x, y;
    scanf("%d %d %d %d", &v, &c, &x, &y);
    int ok = 1;
    while (ok) {

        vector<int> key(c+1);
        graph G(v+1);
        for (int i = 0; i < c; i++) scanf("%d", &key[i]);

        for (int i = 0; i < v - 1; i++) {
            int a, b, l;
            scanf("%d %d %d", &a, &b, &l);
            NODE a_node, b_node;
            a_node.a = a; b_node.a = b;
            a_node.b = b; b_node.b = a;
            a_node.l = l; b_node.l = l;
            G[a].push_back(a_node);
            G[b].push_back(b_node);
        }

        list<NODE> ans;
        turn = 0;
        vector<NODE> prev(G.size()+1);
        vector<int> d(G.size(), -1), din(G.size()+1), dout(G.size()+1);
        vector<bool> fou(G.size()+1, false), need(G.size()+1, false);
        DFS s;
        s.prev = prev;
        s.depth = d;
        s.dfsin = din;
        s.dfsout = dout;
        KEYS k;
        k.found = fou;
        k.needed = need;
        k.key = key;
        dfs(G, x, 0, s);
        if(exists_path(G, x, y, ans, s, k)) {
            if(ans.empty()) printf("0: %d\n", x);
            else {
                printf("%i: ", (int)ans.size());
                list<NODE>::iterator it;
                printf("%d", ans.begin()->a);
                for (it = ans.begin(); it != ans.end(); it++) {
                    printf(" %d", it->b);
                }
                printf("\n");
            }
        }
        else printf("Impossible\n");

        scanf("%d %d %d %d", &v, &c, &x, &y);
        if(v == 0 && c == 0 && x == 0 && y == 0) ok = 0;
    }

    return 0;
}
